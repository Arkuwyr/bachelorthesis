#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_cz_adamrichter_bachelorthesis_MenuActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
