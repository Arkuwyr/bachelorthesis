package cz.adamrichter.bachelorthesis.arcore;

/**
 * Created by adamr on 25. 7. 2017.
 */

import android.hardware.Camera;
import android.opengl.Matrix;

import org.opencv.core.CvType;
import org.opencv.core.MatOfDouble;

public final class CameraAdapter {

    float mFOVY = 45f;
    float mFOVX = 60f;
    int mHeightPx = 480;
    int mWidthPx = 640;
    float mNear = 0.001f;
    float mFar = 50;

    final float[] mProjectionGL = new float[16];
    MatOfDouble mProjectionCV;

    public void setCameraParameters(final Camera.Parameters cameraParameters) {
        mFOVY = cameraParameters.getVerticalViewAngle();
        mFOVX = cameraParameters.getHorizontalViewAngle();
    }


    /**
     * Vytvoří projekční matici na základě udajů o kameře.
     * @return
     */
    public float[] createOpenGLProjection() {
        float right =(float)Math.tan(0.5f * mFOVX * Math.PI / 180f) * mNear;
        float top = right / ((float)mWidthPx / (float)mHeightPx);
        Matrix.frustumM(mProjectionGL, 0,-right, right, -top, top, mNear, mFar);
        return mProjectionGL;
    }

    public MatOfDouble getProjectionCV() {
            if (mProjectionCV == null) {
                mProjectionCV = new MatOfDouble();
                mProjectionCV.create(3, 3, CvType.CV_64FC1);
            }

            final float fovAspectRatio = mFOVX / mFOVY;
            final double diagonalPx = Math.sqrt(
                    (Math.pow(mWidthPx, 2.0) +
                            Math.pow(mWidthPx / fovAspectRatio, 2.0)));
            final double focalLengthPx = 0.5 * diagonalPx / Math.sqrt(
                    Math.pow(Math.tan(0.5 * mFOVX * Math.PI / 180f), 2.0) +
                            Math.pow(Math.tan(0.5 * mFOVY * Math.PI / 180f), 2.0));

            mProjectionCV.put(0, 0, focalLengthPx);
            mProjectionCV.put(0, 1, 0.0);
            mProjectionCV.put(0, 2, 0.5 * mWidthPx);
            mProjectionCV.put(1, 0, 0.0);
            mProjectionCV.put(1, 1, focalLengthPx);
            mProjectionCV.put(1, 2, 0.5 * mHeightPx);
            mProjectionCV.put(2, 0, 0.0);
            mProjectionCV.put(2, 1, 0.0);
            mProjectionCV.put(2, 2, 1.0);
        return mProjectionCV;
    }

    public int getCameraAspectRatio() {
        return mWidthPx/mHeightPx;
    }
}

