package cz.adamrichter.bachelorthesis.arcore;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point3;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.IOException;

import cz.adamrichter.bachelorthesis.R;

import static org.opencv.features2d.Features2d.drawKeypoints;

/**
 * Created by adamr on 24. 7. 2017.
 */

public class Marker {


    private Mat marker;
    private MatOfPoint3f marker3Dcorners;
    private MatOfKeyPoint markerKeypoints;
    private Mat markerDescroptors;
    private ImageView markerView;


    public Marker(Context context, ImageView markerView) {
        Mat markerKP = new Mat();
        marker3Dcorners = new MatOfPoint3f();

        try {
            marker = Utils.loadResource(context,R.drawable.hiro,Imgcodecs.CV_LOAD_IMAGE_COLOR);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FeatureDetector detector2 = FeatureDetector.create(FeatureDetector.DYNAMIC_FAST);
        markerKeypoints = new MatOfKeyPoint();
        markerDescroptors = new Mat();
        detector2.detect(marker, markerKeypoints);




        initMarker3DPoints();


        DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
        extractor.compute(marker, markerKeypoints, markerDescroptors);
        drawKeypoints(marker,markerKeypoints,markerKP);

        Bitmap bm = Bitmap.createBitmap(markerKP.cols(), markerKP.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(markerKP, bm);
        markerView.setImageBitmap(bm);
        markerView.setVisibility(View.INVISIBLE);
        this.markerView = markerView;
    }

    /**
     * vytvoří odpovídající body ve 3D
     */
    private void initMarker3DPoints() {
        marker3Dcorners.fromArray(new Point3(-1, -1, 0.0),new Point3( 1, -1, 0.0),new Point3( 1,  1, 0.0),new Point3(-1,  1, 0.0));
    }

    /**
     * zobrazí náhled markeru s klíčovými body
     */
    public void showMarker(){
        markerView.setVisibility(View.VISIBLE);
    }

    /**
     * skryje náhled markeru s klíčovými body
     */
    public void hideMarker(){
        markerView.setVisibility(View.INVISIBLE);
    }

    /**
     * vrazí deskriptory markeru
     * @return
     */
    public Mat getMarkerDescroptors() {
        return markerDescroptors;
    }

    /**
     * vrací odpovídající body ve 3D
     * @return
     */
    public MatOfPoint3f getMarker3DCorners() {
        return marker3Dcorners;
    }

}
