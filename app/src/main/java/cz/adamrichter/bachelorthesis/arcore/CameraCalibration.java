package cz.adamrichter.bachelorthesis.arcore;

/**
 * Created by adamr on 26. 7. 2017.
 */
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point3;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class CameraCalibration {

    private ArrayList<Mat> imagePoints;
    private ArrayList<Mat> tvecs;
    private ArrayList<Mat> rvecs;
    private MatOfPoint2f imageCorners;
    private Mat distCoeffs;
    private Mat cameraMatrix;
    private MatOfPoint3f obj;
    private ArrayList<Mat> objectPoints;
    private Size frameSize;
    private boolean calibrated = false;
    private boolean found = false;

    public CameraCalibration() {
        imagePoints = new ArrayList<>();
        tvecs = new ArrayList<>();
        rvecs = new ArrayList<>();
        imageCorners = new MatOfPoint2f();
        distCoeffs = new Mat();
        cameraMatrix = new Mat();
        obj = new MatOfPoint3f();
        objectPoints = new ArrayList<>();
        frameSize = new Size();

        for (int i = 0; i < 9 * 6; i++){
            obj.push_back(new MatOfPoint3f(new Point3(i / 9, i % 6, 0.0f)));
        }



    }

    /**
     * Metoda nalezne šachovnici v obraze a zobrazí ji ve snímku
     * @param frame Snímek kde má být nalezena šachovnice
     */
    public void findChesboard(Mat frame) {
        Mat grayImage = new Mat();
        Size boardSize = new Size(9, 6);
        Imgproc.cvtColor(frame, grayImage, Imgproc.COLOR_RGB2GRAY);

        found = Calib3d.findChessboardCorners(grayImage, boardSize, imageCorners, Calib3d.CALIB_CB_ADAPTIVE_THRESH+ Calib3d.CALIB_CB_FILTER_QUADS + Calib3d.CALIB_CB_NORMALIZE_IMAGE + Calib3d.CALIB_CB_FAST_CHECK);

        if (found) {

            TermCriteria term = new TermCriteria(TermCriteria.EPS | TermCriteria.MAX_ITER, 30, 0.1);
            Imgproc.cornerSubPix(grayImage, imageCorners, new Size(11, 11), new Size(-1, -1), term);

            Calib3d.drawChessboardCorners(frame, boardSize, imageCorners, found);
        }

        frameSize = grayImage.size();
    }

    /**
     * Metoda uložení kalibračních dat
     * @return boolean jestli byly informace uloženy
     */
    public boolean takeSnapshot() {
        if (found) {
            imagePoints.add(imageCorners);
            objectPoints.add(obj);
        }
        return found;
    }

    /**
     * Metoda kalibrující kameru
     */
    public void calibrateCamera() {
        cameraMatrix.put(0, 0, 1);
        cameraMatrix.put(1, 1, 1);
        Calib3d.calibrateCamera(objectPoints, imagePoints, frameSize, cameraMatrix, distCoeffs, rvecs, tvecs);
        calibrated = true;
    }

    /**
     * Metoda vracenící informace u tom jestli je kamera kalibrována
     * @return boolena
     */
    public boolean isCalibrated() {
        return calibrated;
    }

    /**
     * metoda vrací informace o kameře
     * @return MatOfDouble
     */
    public MatOfDouble getDistCoeffs() {
        return new MatOfDouble(distCoeffs);
    }
}
