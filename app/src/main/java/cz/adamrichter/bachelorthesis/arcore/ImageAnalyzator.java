package cz.adamrichter.bachelorthesis.arcore;

import android.util.Log;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by adamr on 24. 7. 2017.
 */

public class ImageAnalyzator {

    private FeatureDetector detector = FeatureDetector.create(FeatureDetector.DYNAMIC_FAST);
    private DescriptorMatcher mather = DescriptorMatcher.create( DescriptorMatcher.BRUTEFORCE_HAMMING);
    private DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
    private Marker marker;
    private Mat rotation;
    private CameraAdapter cameraAdapter;
    private float[] GLPoseMatrix;
    private MatOfDouble rVec;
    private MatOfDouble tVec;


    public ImageAnalyzator(Marker marker, CameraAdapter cameraAdapter) {
        this.marker = marker;
        rotation = new MatOfDouble();
        rVec = new MatOfDouble();
        tVec = new MatOfDouble();
        this.cameraAdapter = cameraAdapter;
        GLPoseMatrix = new float[16];
    }

    /**
     * Převede obraz na šedý
     * @param inputframe
     * @return
     */
    public Mat convertToGrayscale(Mat inputframe){
        Mat output = new Mat();
        Imgproc.cvtColor(inputframe, output, Imgproc.COLOR_RGB2GRAY);
        return output;
    }

    /**
     * Převede obraz s alfakanálem na obraz bez alfa
     * @param inputframe
     * @return
     */
    public Mat convertToRGB(Mat inputframe){
        Mat output = new Mat();
        Imgproc.cvtColor(inputframe, output, Imgproc.COLOR_RGBA2RGB);
        return output;
    }

    /**
     * rozmaže obraz
     * @param grayImage
     * @return
     */
    public Mat blurImage(Mat grayImage){
        Mat output = new Mat();
        Imgproc.GaussianBlur(grayImage, output, new Size(5, 5),5);
        return output;
    }

    /**
     * Prahuje obraz pomocí Otsuovy metody
     * @param blurredImage
     * @return
     */
    public Mat tresholdImage(Mat blurredImage){
        Mat output = new Mat();
        int tresh = 255;
        Imgproc.threshold(blurredImage, output, tresh, 255, Imgproc.THRESH_OTSU);
        return output;
    }

    /**
     * Nalezne kontury v obraze
     * @param tresholdImage
     * @return
     */
    public ArrayList<MatOfPoint> findContours(Mat tresholdImage){
        ArrayList<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(tresholdImage, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);
        return contours;
    }

    /**
     * Nalezne a vrátí čtvercové kontury
     * @param contours
     * @return
     */
    public ArrayList<MatOfPoint> detectSqueresFromContours(ArrayList<MatOfPoint> contours) {
        ArrayList<MatOfPoint> squares = new ArrayList<>();
        for (int i=0; i<contours.size();i++) {
            MatOfPoint2f approx = new MatOfPoint2f();
            if (isContourSquare(contours.get(i), approx)){
                squares.add(new MatOfPoint(approx.toArray()));
            }

        }
        return squares;
    }

    /**
     * Rozhodne jestli je kontura čtvercová
     * @param contour
     * @param approx
     * @return
     */
    private boolean isContourSquare(MatOfPoint contour, MatOfPoint2f approx){
        if (!Imgproc.isContourConvex(contour)){
            Imgproc.approxPolyDP(new MatOfPoint2f(contour.toArray()), approx,
                    Imgproc.arcLength(new MatOfPoint2f(contour.toArray()), true) * 0.1, true);
            if (approx.toList().size() == 4 && Math.abs(Imgproc.contourArea(approx)) > 1000) {
                return true;
            }
        }
        return false;
    }

    /**
     * Nalezne klíčové body v obraze
     * @param image
     * @return
     */
    public MatOfKeyPoint detectKeyPoints(Mat image){
        MatOfKeyPoint keypoints = new MatOfKeyPoint();
        detector.detect(image, keypoints);
        return keypoints;
    }


    /**
     * Nalezne v obraze Marker
     * @param contours
     * @param mask
     * @param image
     * @return
     */
    public ArrayList<MatOfPoint> detectMarker(ArrayList<MatOfPoint> contours, Mat mask, Mat image){
        MatOfKeyPoint keypoints = new MatOfKeyPoint();
        ArrayList<MatOfPoint> markerHA = new ArrayList<>();
        for (int i=0; i<contours.size();i++) {
            if (!Imgproc.isContourConvex(contours.get(i))) {
                MatOfPoint2f approx = new MatOfPoint2f();
                Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(i).toArray()), approx,
                        Imgproc.arcLength(new MatOfPoint2f(contours.get(i).toArray()), true) * 0.1, true);
                if (approx.toList().size() == 4 && Math.abs(Imgproc.contourArea(approx)) > 1000 ) {
                    //extrahování ctvrecových oblastí zajmu v obraze
                    Mat roi = new Mat(mask, Imgproc.boundingRect(new MatOfPoint(contours.get(i).toArray())));
                    roi.setTo(new Scalar(255, 255, 255));

                    detector.detect(new Mat(image, Imgproc.boundingRect(new MatOfPoint(contours.get(i).toArray()))), keypoints, mask);

                    MatOfDMatch mMatches3 = new MatOfDMatch();
                    Mat mSceneDescriptors2 = new Mat();

                    extractor.compute(new Mat(image, Imgproc.boundingRect(new MatOfPoint(contours.get(i).toArray()))), keypoints, mSceneDescriptors2);
                    mather.match(mSceneDescriptors2, marker.getMarkerDescroptors(), mMatches3);


                    if (compareImages(mMatches3)) {
                        if (markerHA.isEmpty()){

                            markerHA.add(new MatOfPoint(approx.toArray()));
                        }else {

                            if (Math.abs(Imgproc.contourArea(approx)) <= Math.abs(Imgproc.contourArea(markerHA.get(0)))){
                                markerHA.set(0, new MatOfPoint(approx.toArray()));
                            }
                        }

                    }
                }
            }
        }
        return markerHA;
    }

    /**
     * Porovná dva obrazy na základě deskriptorů
     * @param mMatches
     * @return
     */
    private boolean compareImages(MatOfDMatch mMatches) {
        List<DMatch> matchesList = mMatches.toList();
        if (matchesList.size() < 4) {
            return false;
        }
        double maxDist = 0.0;
        double minDist = Double.MAX_VALUE;
        for(DMatch match : matchesList) {
            double dist = match.distance;
            if (dist < minDist) {
                minDist = dist;
            }
            if (dist > maxDist) {
                maxDist = dist;
            }
        }
        if (minDist > 50.0) {
            return false;
        } else if (minDist > 25.0) {
            return false;
        }
        return true;
    }

    /**
     * Převede data z vektoru do pozice OpenGL
     * @return
     */
    public float[] getCameraPose(){

        if (rVec.empty() || tVec.empty()){
            return null;
        }


        double[] rVecArray = rVec.toArray();
        if (rVecArray[0] > 0) {
            rVecArray[0] *= -1.0;
        }


        rVecArray[0] *= -1.0;
        rVec.fromArray(rVecArray);
        Calib3d.Rodrigues(rVec, rotation);

        double[] tVecArray = tVec.toArray();
        GLPoseMatrix[0]  =  (float) rotation.get(0, 0)[0];
        GLPoseMatrix[1]  =  (float) rotation.get(0, 1)[0];
        GLPoseMatrix[2]  =  (float) rotation.get(0, 2)[0];
        GLPoseMatrix[3]  =  0f;
        GLPoseMatrix[4]  =  (float) rotation.get(1, 0)[0];
        GLPoseMatrix[5]  =  (float) rotation.get(1, 1)[0];
        GLPoseMatrix[6]  =  (float) rotation.get(1, 2)[0];
        GLPoseMatrix[7]  =  0f;
        GLPoseMatrix[8]  =  (float) rotation.get(2, 0)[0];
        GLPoseMatrix[9]  =  (float) rotation.get(2, 1)[0];
        GLPoseMatrix[10] =  (float) rotation.get(2, 2)[0];
        GLPoseMatrix[11] =  0f;
        GLPoseMatrix[12] =  (float)tVecArray[0];
        GLPoseMatrix[13] = -(float)tVecArray[1];
        GLPoseMatrix[14] = -(float)tVecArray[2];
        GLPoseMatrix[15] =  1f;
        return  GLPoseMatrix;
}

    /**
     * Vypočte pozici pozorovatele vzhledem k markeru
     * @param markerContour
     * @param cc
     */
    public void compute3DPose(MatOfPoint markerContour, CameraCalibration cc) {
        rVec = new MatOfDouble();
        tVec = new MatOfDouble();
        Calib3d.solvePnP(marker.getMarker3DCorners(),new MatOfPoint2f(markerContour.toArray()),cameraAdapter.getProjectionCV(),cc.getDistCoeffs(), rVec, tVec);
    }
}
