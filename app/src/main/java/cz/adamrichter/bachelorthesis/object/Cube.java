package cz.adamrichter.bachelorthesis.object;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

/**
 * Created by Adam on 19. 8. 2017.
 */

public class Cube {


    private ByteBuffer COLORS;
    private ByteBuffer VERTEXES;
    private ByteBuffer FACES;

    public Cube() {
        initVertexes();

        initColours();

        initFaces();
    }

    /**
     * Inicializuje stěny
     */
    private void initFaces() {
        FACES = ByteBuffer.allocateDirect(36);
        FACES.put(new byte[] {
                0,  1,  2,  2,  3,  0,
                // horni
                3,  2,  6,  6,  7,  3,
                // zadni
                7,  6,  5,  5,  4,  7,
                // horni
                4,  5,  1,  1,  0,  4,
                // predni
                4,  0,  3,  3,  7,  4,
                // leva
                1,  5,  6,  6,  2,  1
                // prava
        });
        FACES.position(0);
    }

    /**
     * Inicializuje barvy
     */
    private void initColours() {
        COLORS = ByteBuffer.allocateDirect(32);
        COLORS.put(new byte[] {
                // Front.
                (byte)255, 0, 0, (byte)255,        // red
                (byte)255, (byte)255, 0, (byte)255, // yellow
                (byte)255, (byte)255, 0, (byte)255, // yellow
                (byte)255, 0, 0, (byte)255,        // red
                // Back.
                0, (byte)255, 0, (byte)255,        // green
                0, 0, (byte)255, (byte)255,        // blue
                0, 0, (byte)255, (byte) 255,        // blue
                0, (byte)255, 0, (byte) 255         // green
        });
        COLORS.position(0);
    }

    /**
     * Inicializuje vertexy
     */
    private void initVertexes() {
        VERTEXES = ByteBuffer.allocateDirect(96);
        VERTEXES.order(ByteOrder.nativeOrder());
        VERTEXES.asFloatBuffer().put(new float[] {
                // vrchni vertexy
                -0.5f, -0.5f,  1f,
                0.5f, -0.5f,  1f,
                0.5f,  0.5f,  1,
                -0.5f,  0.5f,  1f,
                //spodní vertexy
                -0.5f, -0.5f, 0f,
                0.5f, -0.5f, 0f,
                0.5f,  0.5f, 0f,
                -0.5f,  0.5f, 0f
        });
        VERTEXES.position(0);
    }


    /**
     *  Vykreslí kostku
     * @param gl
     */
    public void draw(GL10 gl) {
        gl.glVertexPointer(3, GL11.GL_FLOAT, 0, VERTEXES);
        gl.glColorPointer(4, GL11.GL_UNSIGNED_BYTE, 0, COLORS);

        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_COLOR_ARRAY);

        gl.glLineWidth(5);
        gl.glDrawElements(GL10.GL_TRIANGLES, 36,
                GL10.GL_UNSIGNED_BYTE, FACES);
    }

}
