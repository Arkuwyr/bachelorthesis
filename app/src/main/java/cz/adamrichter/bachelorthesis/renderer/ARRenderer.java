package cz.adamrichter.bachelorthesis.renderer;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import cz.adamrichter.bachelorthesis.activities.ARPreviewActivity;
import cz.adamrichter.bachelorthesis.arcore.CameraAdapter;
import cz.adamrichter.bachelorthesis.arcore.ImageAnalyzator;
import cz.adamrichter.bachelorthesis.object.Cube;

/**
 * Created by adamr on 20. 7. 2017.
 */

public class ARRenderer implements GLSurfaceView.Renderer {
    private ImageAnalyzator ia;
    private CameraAdapter ca;

    private int displayWidth;
    private int displayHeight;
    private Cube cube;



    public ARRenderer(CameraAdapter ca) {
        this.ca = ca;
        cube = new Cube();
    }

    @Override
    public void onSurfaceCreated(final GL10 gl,final EGLConfig config) {
        gl.glClearColor(0f, 0f, 0f, 0f);
        gl.glEnable(GL10.GL_CULL_FACE);
    }



    @Override
    public void onSurfaceChanged(final GL10 gl, final int width, final int height) {
        displayWidth = width;
        displayHeight = height;
    }

    @Override
    public void onDrawFrame(final GL10 gl) {

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glEnable(gl.GL_DEPTH_TEST);


        if ((ca == null)||(ia == null)||(!ARPreviewActivity.isRendering3D())) {
            return;
        }

        float[] cvCamPose = ia.getCameraPose();
        if (cvCamPose == null) {
            return;
        }

        setupScreen(gl);

        gl.glMatrixMode(GL10.GL_PROJECTION);
        float[] projection = ca.createOpenGLProjection();

        gl.glLoadMatrixf(projection, 0);

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadMatrixf(cvCamPose, 0);



        cube.draw(gl);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
    }

    private void setupScreen(GL10 gl) {
        int viewWidth = (int)(displayHeight * (640/480));
        int horShift = (displayWidth - viewWidth) / 2;
        gl.glViewport(horShift, 0, viewWidth, displayHeight);
    }

    public void setImageAnalyzator(ImageAnalyzator imageAnalyzator) {
        this.ia = imageAnalyzator;
    }
}

