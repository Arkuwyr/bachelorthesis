package cz.adamrichter.bachelorthesis.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import cz.adamrichter.bachelorthesis.R;

/**
 * Created by Adam on 19. 8. 2017.
 */

public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Button openAr = (Button) findViewById(R.id.openAR);
        openAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, ARPreviewActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
