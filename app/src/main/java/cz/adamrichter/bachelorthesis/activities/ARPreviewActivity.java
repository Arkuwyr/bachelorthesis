package cz.adamrichter.bachelorthesis.activities;


import android.app.Activity;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

import cz.adamrichter.bachelorthesis.renderer.ARRenderer;
import cz.adamrichter.bachelorthesis.arcore.CameraAdapter;
import cz.adamrichter.bachelorthesis.arcore.CameraCalibration;
import cz.adamrichter.bachelorthesis.arcore.ImageAnalyzator;
import cz.adamrichter.bachelorthesis.arcore.Marker;
import cz.adamrichter.bachelorthesis.R;

import static org.opencv.core.CvType.CV_8U;
import static org.opencv.features2d.Features2d.drawKeypoints;


public class ARPreviewActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private enum State {
        DEFAULT, GRAYSCALE, BLURED, TRESHOLD, CONTOURS, RECTS, KEYPOINTS, DETECT_MARKER,CALIBRATE,DISPLAY_AUGMENTED_REALITY,TEST;


        private static State[] values = values();
        public State nextState()
        {
            return values[(this.ordinal()+1) % values.length];
        }

    }

    private CameraBridgeViewBase cameraView;
    private static State currentState = State.DEFAULT;
    private GLSurfaceView glview;
    private ImageAnalyzator ia;
    private Marker marker;
    private ARRenderer renderer;
    private CameraCalibration cc;
    private int SNAPSHOT_COUNT = 5;
    private CameraAdapter ca;



    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    marker = new Marker(getBaseContext(), (ImageView) findViewById(R.id.markerPreview));
                    cc = new CameraCalibration();


                    cameraView.enableView();
                    ca = new CameraAdapter();
                    ia = new ImageAnalyzator(marker, ca);
                    renderer.setImageAnalyzator(ia);

                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_arpreview);

        cameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
        cameraView.setVisibility(SurfaceView.VISIBLE);
        cameraView.enableFpsMeter();
        cameraView.setCvCameraViewListener(this);
        cameraView.setMaxFrameSize(640 , 480);


        glview = (GLSurfaceView) findViewById(R.id.opengl_view);

        ca = new CameraAdapter();
        glview.setEGLConfigChooser(8, 8, 8, 8, 0, 0);
        glview.getHolder().setFormat(PixelFormat.TRANSPARENT);
        renderer = new ARRenderer(ca);
        glview.setRenderer(renderer);



        final Button button = (Button) findViewById(R.id.changeState);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (currentState) {
                    case DEFAULT:
                        button.setText(R.string.button_state_1);
                        currentState = currentState.nextState();
                        return;
                    case GRAYSCALE:
                        button.setText(R.string.button_state_2);
                        currentState = currentState.nextState();
                        return;
                    case BLURED:
                        button.setText(R.string.button_state_3);
                        currentState = currentState.nextState();
                        return;
                    case TRESHOLD:
                        button.setText(R.string.button_state_4);
                        currentState = currentState.nextState();
                        return;
                    case CONTOURS:
                        button.setText(R.string.button_state_5);
                        currentState = currentState.nextState();
                        return;
                    case RECTS:
                        button.setText(R.string.button_state_6);
                        currentState = currentState.nextState();
                        return;
                    case KEYPOINTS:
                        marker.showMarker();
                        button.setText(R.string.button_state_7);
                        currentState = currentState.nextState();
                        return;
                    case DETECT_MARKER:
                        button.setText("Take Snapshot");
                        marker.hideMarker();
                        currentState = currentState.nextState();
                        return;
                    case CALIBRATE:

                        if (SNAPSHOT_COUNT >1){
                            Toast.makeText(ARPreviewActivity.this, "SnapshotTaken", Toast.LENGTH_SHORT).show();
                            cc.takeSnapshot();
                            SNAPSHOT_COUNT--;
                        }else if (SNAPSHOT_COUNT ==1){
                            button.setText("Calibrate");
                            cc.calibrateCamera();
                            if (cc.isCalibrated()){
                                Toast.makeText(ARPreviewActivity.this, "Calibrated", Toast.LENGTH_SHORT).show();
                                button.setText(getResources().getText(R.string.button_state_8));

                            }
                            SNAPSHOT_COUNT--;
                        }else if (SNAPSHOT_COUNT==0){
                            button.setText(R.string.button_state_8);
                            currentState = currentState.nextState();
                        }
                        return;
                    case DISPLAY_AUGMENTED_REALITY:
                        button.setText(R.string.button_state_0);
                        currentState = currentState.nextState();
                        return;
                    default:
                        button.setText(R.string.button_state_0);
                        currentState = currentState.nextState();
                        return;
                }
            }
        });

        final Camera camera;
        camera = Camera.open(0);
        final Camera.Parameters parameters = camera.getParameters();
        ca.setCameraParameters(parameters);
    }



    @Override
    public void onResume()
    {
        super.onResume();
        glview.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        glview.onPause();
        if (cameraView != null)
            cameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (cameraView != null)
            cameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat grayImage;
        Mat rgbImage = ia.convertToRGB(inputFrame.rgba());
        Mat blurredImage;
        Mat tresholdImage;
        ArrayList<MatOfPoint> contours;
        ArrayList<MatOfPoint> squeres;
        MatOfPoint markerContour;
        MatOfKeyPoint keypoints;
        Mat mask = Mat.zeros(inputFrame.rgba().size(), CV_8U);


        switch (currentState) {
            case DEFAULT:
                //default view
                return inputFrame.rgba();
            case GRAYSCALE:
                //grayscale
                return ia.convertToGrayscale(inputFrame.rgba());
            case BLURED:
                //blurred
                grayImage = ia.convertToGrayscale(inputFrame.rgba());
                return ia.blurImage(grayImage);
            case TRESHOLD:
                //treshold
                grayImage = ia.convertToGrayscale(inputFrame.rgba());
                blurredImage = ia.blurImage(grayImage);
                return ia.tresholdImage(blurredImage);
            case CONTOURS:
                //contours
                grayImage = ia.convertToGrayscale(inputFrame.rgba());
                blurredImage = ia.blurImage(grayImage);
                tresholdImage = ia.tresholdImage(blurredImage);
                contours = ia.findContours(tresholdImage);
                Imgproc.drawContours(rgbImage, contours, -1, new Scalar(255, 0, 0));
            return rgbImage;
            case RECTS:
                // rects
                grayImage = ia.convertToGrayscale(inputFrame.rgba());
                blurredImage = ia.blurImage(grayImage);
                tresholdImage = ia.tresholdImage(blurredImage);
                contours = ia.findContours(tresholdImage);
                squeres = ia.detectSqueresFromContours(contours);

                Imgproc.drawContours(rgbImage, squeres, -1, new Scalar(0, 255, 0));
                return rgbImage;
            case KEYPOINTS:
                //keypoints in rects
                grayImage = ia.convertToGrayscale(inputFrame.rgba());
                blurredImage = ia.blurImage(grayImage);
                tresholdImage = ia.tresholdImage(blurredImage);
                contours = ia.findContours(tresholdImage);
                squeres = ia.detectSqueresFromContours(contours);
                keypoints = ia.detectKeyPoints(grayImage);
                drawKeypoints(rgbImage, keypoints, rgbImage);
                Imgproc.drawContours(rgbImage, squeres, -1, new Scalar(255,128,  0));
                return rgbImage;
            case DETECT_MARKER:
                //detect marker
                grayImage = ia.convertToGrayscale(inputFrame.rgba());
                blurredImage = ia.blurImage(grayImage);
                tresholdImage = ia.tresholdImage(blurredImage);
                contours = ia.findContours(tresholdImage);
                Imgproc.drawContours(rgbImage, ia.detectMarker(contours, mask, grayImage), 0, new Scalar(255,255,  0));
                return rgbImage;
            case CALIBRATE:
                cc.findChesboard(rgbImage);

            return rgbImage;
            case DISPLAY_AUGMENTED_REALITY:
                grayImage = ia.convertToGrayscale(inputFrame.rgba());
                blurredImage = ia.blurImage(grayImage);
                tresholdImage = ia.tresholdImage(blurredImage);
                contours = ia.findContours(tresholdImage);
                ArrayList<MatOfPoint> container = ia.detectMarker(contours, mask, grayImage);
                if (!container.isEmpty()){
                    markerContour = container.get(0);
                    ia.compute3DPose(markerContour, cc);
                    Imgproc.drawContours(rgbImage, container, 0, new Scalar(128,255,  0));
                }
                return rgbImage;
            default:
                return inputFrame.rgba();
        }
    }

    /**
     *  předá informaci o tom jestli už má být vykreslován objekt
     * @return boolean
     */
    public static boolean isRendering3D(){
        if(currentState == State.DISPLAY_AUGMENTED_REALITY ){
            return true;
        }
        return false;
    }
}
